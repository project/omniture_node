<?php

/**
 * @file
 * Schemas for Omniture node parameters and link profiles.
 *
 * @author: Alexander Wilde (al.ex)
 */

/**
 * Provides ordered list of Omniture node parameter names and metadata settings.
 *
 * Parameters will be stored as key/value pairs with the parameter name's index in this array as key.
 *
 * Example:
 * 1 => "Special offer" (key/value stored in DB)
 * "s.pageName" => "Special offer" (translated output for Omniture tracking)
 *
 *
 * @param $name
 *   Name of schema to be loaded.
 * @return
 *   Array of selected schema.
 */
function _get_omniture_node_parameter_schema($name = 'node') {

  /* Schema for Omniture node parameters */
  $node = array(
    array(
      'name' => 's.pageName',
      'element' => array(
        '#title' => 'Page Name',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.channel',
      'element' => array(
        '#title' => 'Channel name',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.hier1',
      'element' => array(
        '#title' => 's.hier1',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.prop1',
      'element' => array(
        '#title' => 's.prop1',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.prop2',
      'element' => array(
        '#title' => 's.prop2',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.prop3',
      'element' => array(
        '#title' => 's.prop3',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar1',
      'element' => array(
        '#title' => 's.eVar1',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar2',
      'element' => array(
        '#title' => 's.eVar2',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar3',
      'element' => array(
        '#title' => 's.eVar3',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar4',
      'element' => array(
        '#title' => 's.eVar4',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar5',
      'element' => array(
        '#title' => 's.eVar5',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar6',
      'element' => array(
        '#title' => 's.eVar6',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar7',
      'element' => array(
        '#title' => 's.eVar7',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar8',
      'element' => array(
        '#title' => 's.eVar8',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar9',
      'element' => array(
        '#title' => 's.eVar9',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar10',
      'element' => array(
        '#title' => 's.eVar10',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar11',
      'element' => array(
        '#title' => 's.eVar11',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.eVar12',
      'element' => array(
        '#title' => 's.eVar12',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.events',
      'element' => array(
        '#title' => 's.events',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.campaign',
      'element' => array(
        '#title' => 's.campaign',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
    array(
      'name' => 's.tl',
      'element' => array(
        '#title' => 's.tl',
        '#description' => '',
        '#type' => 'textfield',
        '#size' => 50,
        '#default_value' => '',
      ),
    ),
  
  );

  /* Schema for Omniture link profiles */
  $linkprofile = array(
    array(
      'name' => 'name',
      'element' => array(
        '#type' => 'textfield',
        '#title' => t('Profile name'),
        '#weight' => 0,
      ),
    ),
    array(
      'name' => 'eVar_key',
      'element' => array(
        '#type' => 'select',
        '#title' => t('eVar key'),
        '#options' => array(
          '' => t('Pull to select'),
          'eVar1' => 'eVar1',
          'eVar2' => 'eVar2',
          'eVar3' => 'eVar3',
          'eVar4' => 'eVar4',
          'eVar5' => 'eVar5',
          'eVar6' => 'eVar6',
          'eVar7' => 'eVar7',
          'eVar8' => 'eVar8',
          'eVar9' => 'eVar9',
          'eVar10' => 'eVar10',
          'eVar11' => 'eVar11',
          'eVar12' => 'eVar12',
        ),
      ),
    ),
    array(
      'name' => 'eVar_value',
      'element' => array(
        '#type' => 'textfield',
        '#title' => t('eVar value'),
      ),
    ),
    array(
      'name' => 's.events',
      'element' => array(
        '#type' => 'select',
        '#title' => t('s.events'),
        '#options' => array(
          '' => t('Pull to select'),
          'event1' => 'event1',
          'event2' => 'event2',
          'event3' => 'event3',
          'event4' => 'event4',
          'event5' => 'event5',
          'event6' => 'event6',
        ),
      ),
    ),
    array(
      'name' => 's.tl',
      'element' => array(
        '#type' => 'textfield',
        '#title' => t('s.tl'),
      ),
    ),
  );
  
  return $$name;
}