
-- SUMMARY --

The Omniture Node module extends the Omniture Integration ("omniture") module.
It allows to store Omniture parameters for nodes. 

When a node is being viewed, its custom node parameters get added to the 
SiteCatalyst javascript output.

For a full description of the module, visit the project page:
  http://drupal.org/project/omniture_node

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/omniture_node


-- REQUIREMENTS --

This module requires version 6.x-2.x-dev of the Omniture Integration ("omniture") module. 


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  omniture_node module:

  - administer omniture node module

    Users in roles with the "administer omniture node module" permission can enable
    Omniture parameters for specific node content types.

  - edit omniture node parameters

    Users in roles with the "edit omniture node parameters" permission can edit 
    Omniture parameters of nodes.

* Enable 'Omniture parameters' for node content types in Administer >> Content management >> 
  Content types:
  
    1) Edit a content type.
    
    2) Under fieldset 'Omniture', enable Omniture parameters.
    
    3) Optionally set which parameters are required.


-- CUSTOMIZATION --


-- TROUBLESHOOTING --


-- FAQ --


-- CONTACT --

Current maintainers:
* Alexander Wilde (al.ex) - http://drupal.org/user/160931

This project has been sponsored by:
* Alexo web development
  Custom module development and Drupal consulting. Visit http://www.alexo.it for more information.

* Jaspersoft
  Open Source Business Intelligence Software and Services. Visit http://www.jaspersoft.com for more information.
